# SkillNode

> Godot 3.4.2

角色的功能逻辑，按照攻击、施放技能时的阶段的逻辑执行功能。分为以下几个阶段：

准备阶段，开始抬手执行功能 > 开始功能阶段 > 持续阶段 > 功能结束阶段，开始放手 > 完全结束 > 功能冷却阶段 > 刷新可使用

每次执行都会发出一个信号，通过连接这个信号去执行不同阶段的功能，即实现了角色的功能逻辑